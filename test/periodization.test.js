const periodization = require('../index')
const assert = require('assert')

describe("periodization", function() {
  it(`[1,2,3,4,5,6,7,8] -> "1-8"`, async () =>
    assert.equal(await periodization([1,2,3,4,5,6,7,8]), "1-8")
  )

  it(`[1,3,4,5,6,7,8] -> "1,3-8"`, async () =>
    assert.equal(await periodization([1,3,4,5,6,7,8]), "1,3-8")
  )

  it(`[1,3,4,5,6,7,8,10,11,12] -> "1,3-8,10-12"`, async () =>
    assert.equal(await periodization([1,3,4,5,6,7,8,10,11,12]), "1,3-8,10-12")
  )

  it(`[1,2,3] -> "1-3"`, async () =>
    assert.equal(await periodization([1,2,3]), "1-3")
  )

  it(`[1,2] -> "1,2"`, async () =>
    assert.equal(await periodization([1,2]), "1,2")
  )

  it(`[1,2,4] -> "1,2,4"`, async () =>
    assert.equal(await periodization([1,2,4]), "1,2,4")
  )

  it(`[1,2,4,5,6] -> "1,2,4-6"`, async () =>
    assert.equal(await periodization([1,2,4,5,6]), "1,2,4-6")
  )

  it(`[1,2,3,7,8,9,15,17,19,20,21] -> "1-3,7-9,15,17,19-21"`, async () =>
    assert.equal(await periodization([1,2,3,7,8,9,15,17,19,20,21]), "1-3,7-9,15,17,19-21")
  )

  it(`[1,2,3,4,5,6,100,1091,1999,2000,2001,2002] -> "1-6,100,1091,1999-2002"`, async () =>
    assert.equal(await periodization([1,2,3,4,5,6,100,1091,1999,2000,2001,2002]), "1-6,100,1091,1999-2002")
  )

  it(`[1] -> "1"`, async () =>
    assert.equal(await periodization([1]), "1")
  )

  it(`[1,3,5,7,9,11] -> "1,3,5,7,9,11"`, async () =>
    assert.equal(await periodization([1,3,5,7,9,11]), "1,3,5,7,9,11")
  )
})
