/**
 * @param {integer[]} arr - Числа в массиве всегда целые, положительные и отсортированы по возрастанию.
 * @return {String} - Строка с промежутками
 */
async function periodization(arr) {
  let result = `${arr[0]}`
  let intermediate = [arr[0]]

  for (let i = 1; i <= arr.length; i++) {
    const item = arr[i]

    if (arr[i - 1] == item - 1) {
      intermediate.push(item)
      continue
    }

    if (intermediate.length > 2) {
      result += `-${intermediate[intermediate.length - 1]}`
    }

    if (intermediate.length == 2) {
      result += `,${intermediate[intermediate.length - 1]}`
    }

    if (item !== undefined) {
      result += `,${item}`
    }

    intermediate = [item]
  }
  return result
}

module.exports = periodization
